import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../components/home/home.vue';

Vue.use(VueRouter)

const routes = [
	{
		path: '/home',
		name: 'Home',
		component: Home
	}
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
	scrollBehavior (to) {
		document.getElementById('app').scrollIntoView();
		if (to.hash) {
			return {
				selector: to.hash,
				behavior: 'smooth',
				return: {x: 0, y: 0}
			}
		}
	}
});

export default router
